#WordPress UI Kit

Este un kit de diseño en formato SVG para Inkscape u otro software de diseño vectorial, que incluye los componentes más comunes para diseñar interfaces de plugins o temas para WordPress.

Se distribuye con licencia GPL y eres libre de modificarlo, distribuirlo, compartirlo y adaptarlo siempre que mentengas la licencia y la autoría intactas.

Versión 1.0
© 2022 Hector Asencio

